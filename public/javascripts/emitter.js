var $kick = $("#kick"),
    $stop = $("#stop"),
    timer,
    timerCount = [],
    flightData = {},
    token = "",
    $emitter = $("#emitter"),
    flightLogs = $emitter.data('json'),
    config = $emitter.data('config');
/**
 * 日付をフォーマットする
 * @param  {Date}   date     日付
 * @param  {String} [format] フォーマット
 * @return {String}          フォーマット済み日付
 */
var formatDate = function (date, format) {
 if (!format) format = 'YYYY-MM-DD hh:mm:ss';
 format = format.replace(/YYYY/g, date.getFullYear());
 format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
 format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
 format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
 format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
 format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
 if (format.match(/S/g)) {
   var milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
   var length = format.match(/S/g).length;
   for (var i = 0; i < length; i++) format = format.replace(/S/, milliSeconds.substring(i, i + 1));
 }
 return format;
};


login().done(function(result) {
  token = result.data.token;
  initBody(token);
}).fail(function(result) {
  token = result.data.token;
});

$kick.on('click', function(e) {
  var i = 0,
      max = flightLogs.length;
  timerCount.push(timer = setInterval(function() {
    if (i < max) {
      sendLog(i);
    } else {
      stopTimer();
    }
    i++;
  }, 3000));
});

$stop.on('click', function(e) {
  stopTimer();
});

function initBody(token) {
  for (var i = 0; i < flightLogs.length; i++) {
    var currentTime = formatDate(new Date());
    flightLogs[i].token = token;
    flightLogs[i].id = flightLogs[i].flight_id;
    delete flightLogs[i].flight_id;
    delete flightLogs[i].user_id;
    delete flightLogs[i].created_at;
    delete flightLogs[i].updated_at;
    delete flightLogs[i].go_home;
    flightLogs[i].fstarted_at = currentTime;
    if(i === flightLogs.length - 1) {
      flightLogs[i].plan_status = 0;
    } else {
      flightLogs[i].plan_status = 1;
    }
  }
}

function login() {
  return $.ajax({
    type: "POST",
    url: "http://" + config.server + "/api/login",
    crossDomain: true,
    data: { email: "drone@gmail.com", password: "123123" },
    dataType: 'json',
    success: function (data) {
    },
    error: function (data) {
    }
  });
}

function sendLog(i) {
  console.log(i + "回目");
  console.log(flightLogs[i]);
  $.ajax({
    type: "POST",
    url: "http://" + config.server + "/api/flight/log",
    crossDomain: true,
    data: flightLogs[i],
    dataType: 'json',
    success: function (data) {
    },
    error: function (data) {
    }
  });
}

function stopTimer() {
  if(timerCount.length > 0) {
    clearInterval(timerCount.shift());
  }
  console.log("Stopped");
}
