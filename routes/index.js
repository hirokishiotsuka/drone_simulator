var express = require('express');
var mysql = require('mysql2');
var router = express.Router();
var app = express();
var config = require('../config.json')[app.get('env')];

var connection = mysql.createConnection({
  host: config.db_host || '127.0.0.1',
  user: config.db_user || 'homestead',
  password: config.db_pass || 'secret',
  database: config.db_name || 'drone'
});

/* GET home page. */
router.get('/', function(req, res, next) {
  connection.query('SELECT * FROM flight_logs WHERE flight_id = "'+ config.flight_id +'" AND fstarted_at = "' + config.fstarted_at + '"', function (err, rows) {
    res.render('index', { title: 'Drone Flight Send Log API Simulator', flightLogs: rows, config: config });
  });
});
/* GET home page. */
router.get('/update', function(req, res, next) {
  // console.log(req.query['id']);
  connection.query('SELECT * FROM flight_logs WHERE flight_id = "' + req.query['id'] + '" AND fstarted_at = (SELECT fstarted_at FROM flight_logs where flight_id = "' + req.query['id'] + '" ORDER BY fstarted_at DESC LIMIT 1)', function (err, rows) {
    res.send({ flightLogs: rows });
  });
});

/* GET home page. */
router.get('/flights', function(req, res, next) {
  connection.query('SELECT * FROM flights', function (err, rows) {
    res.send({ flights: rows });
  });
});

module.exports = router;
